/*39)  У заданому масиві a2(n) кожний додатний елемент замінити квадратом
його індексу*/
#include <iostream>
#include <stdlib.h>
#include <windows.h>
#include <math.h>
using namespace std;
int main(){
	system("cls");
	int i,j,n,k1,k2;
	int *a;
	cout<<" Kilkist elementov in odnomernomy masive ";
	cin>>n;
	a= (int*)malloc((n*sizeof(int)));
	if(!a){
		printf("Eror pri videlenie memory\n");
		system("pause");
		return 0;
	}
	cout<<"\nInput elements masive("<<n<<") "<<endl;
	for(i=0;i<n;i++){
	
		cin>>a[i];
		
	}
	system("cls");
	printf("Masive a( %d): \n",n);
	for(i=0;i<n;i++){
	
		cout << a[i] << " ";
		
	}
	for(i=0;i<n;i++){
		if(a[i]>0){
			a[i]=pow(i,2);
		}
	}
	cout<<endl;
	printf("Zmineniy masive a( %d): \n",n);
	for(i=0;i<n;i++){
	
		cout << a[i] << " ";
		
	}
	//звільняємо пам'ять
	free(a);
	system("pause");
	return 0;
}