#include <iostream>
#include <windows.h>
#include <math.h>
using namespace std;

int main(){
	system("cls");
	int n, i, z,k=0,average;
	float sum;
	cout<<" Kilkist elementov in odnomernomy masive ";
	cin>>n;
	int *a = new int[n];
	float *s = new float;
	if(!a){
		printf("Eror pri videlenie memory\n");
		system("pause");
		return 0;
	}
	printf("\n Input znachenya z = ");
	cin>>z;
	
	printf("\n Masive a(%d): ",n);
	for(i=0;i<n;i++){
		a[i]=rand()%100-50;
		cout<<a[i]<<" ";
	}
	cout<<endl;
	*s=0;
	average=0;
	for(i=0;i<n;i++){
		if(a[i]>z){
			k++;
			average=average+a[i];
		}
	}
	average=average/k;
	for(i=0;i<n;i++){
		if(a[i]>z){
			sum=sum+(pow((a[i]-average),2));
		}
	}
	*s=sqrt(sum/k);
	printf("\nAverage kvadratichnoe = %5.2f \n",*s);
	delete [] a;
	delete [] s;
	system("pause");
	return 0;
}
