/*19.  В заданий багатовимірній матриці g19(3,6,3) знайти середнє квадратичне
значення усіх елементів. Вивести вихідну матрицю та знайдене середнє
квадратичне значення*/
#include <iostream>
#include <windows.h>
#include <math.h>
using namespace std;

void gotoxy(int xpos, int ypos){
	COORD scrn;
	HANDLE hOuput =GetStdHandle(STD_OUTPUT_HANDLE);
	scrn.X = xpos;
	scrn.Y = ypos;
	SetConsoleCursorPosition(hOuput,scrn);
}

int main(){
	system("cls");
	const int  F=4;
	int i,j,k1,n=5, m=5, k=5;
	float sum,average;
	int *a = new int[n*m*k];
	float *s = new float;
	
	if(!a){
		printf("Eror pri videlenie memory\n");
		system("pause");
		return 0;
	}
	average=0;
	for(i=0;i<n;i++){
		for(j=0;j<m;j++){
			for(k1=0;k1<k;k1++){
				a[i*m+j*n+k1] = rand()%100-50;
				average=average+a[i*m+j*n+k1];
				gotoxy(((j+k1)*F+1),(((1+i)*m)-j));
				cout<<a[i*m+j*n+k1]<<" ";
			}
		}
		cout<<endl;	
	}
	average=average/125;
	*s=0;
	for(i=0;i<n;i++){
		for(j=0;j<m;j++){
			for(k1=0;k1<k;k1++){
				sum=sum+(pow((a[i*m+j*n+k1]-average),2));
			}
		}
	}
	*s=sqrt(sum/k);
	gotoxy(3,27);
	cout<<"\nAverage kvadratichnoe =  "<<*s;
	
	cout<<endl;
	gotoxy(3,30);
	delete [] a;
	delete [] s;
	system("pause");
	return 0;
}