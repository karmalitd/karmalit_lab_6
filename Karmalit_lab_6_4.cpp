/*29.  Створити та вивести одномірний масив з сум від’ємних елементів кожної
парної діагоналі, що паралельні побічній діагоналі матриці v45(n,m).
*/
#include <iostream>
#include <windows.h>
#include <math.h>
#include <stdlib.h>
using namespace std;

void gotoxy(int xpos, int ypos){
	COORD scrn;
	HANDLE hOuput =GetStdHandle(STD_OUTPUT_HANDLE);
	scrn.X = xpos;
	scrn.Y = ypos;
	SetConsoleCursorPosition(hOuput,scrn);
}
int main(){
	system("cls");
	int i, j,n,m, k,p, sum;
	int *a;
	
	cout<<"Input kolvo radkiv = ";
	cin>>n;
	cout<<"Input kolvo stopchikiv = ";
	cin>>m;
	int b[m*n];
	a=(int *)calloc(n*m,sizeof(int));//виділяємо динамічну пам'ять
	if(!a){
		printf("Eror pri videlenie memory\n");
		system("pause");
		return 0;
	}
	for (i=0; i<n; i++)
		for (j=0; j<m; j++)
			*(a+i*m+j)=rand()%100-50;
	
	cout<<"Matrica a["<<n<<"x"<<m<<"] \n";
	for (i=0; i<n; i++)
	{
		for (j=0; j<m; j++)
			printf(" %3d",*(a+i*m+j));
		cout<<endl;
	}
	cout<<endl;
	
	cout<<" Sum = ";
	for (k = 0; k < (n+m-1); k++){  
   		if (k%2==0){
   			sum=0;
   			if(k<n){   
				i=n-1; 
				j=m-k-1;
			}
	
   			else{
				i=n-(k-m+2); 
				j=0;
			}

   		for (; (0<=i)&(j<m);i--,j++){ 
   			if(*(a+i*m+j)<0){
			   sum+=*(a+i*m+j);
			}
   		}
		b[p]=sum;
    	cout<<b[p]<<" ";
		p++;

		}
	}
			

	
	free(a);
	system("pause");
	return 0;
}